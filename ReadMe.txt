##################
# 1. Description #
##################
	-	Scripts are written for automated browser-based tests for Confluence Cloud: https://donghoang.atlassian.net/login
	-	Using selenium-java-2.4.5.
	- 	Basically, the scripts will be executed with the following flows:
		1. Log in with configured user-name and password.
		2. Create pages with selection page type.
		3. Find existing pages and set restriction on it.

#######################
# 2. Script structure # the project is structured with the following folders:
#######################
a. src: this folder contains:
	- automationTest:	There are 3 scripts, each script for each testcase and the TC_RunAll.java is used to run all TC. 
	- PageObjects:		Define the page objects as: 	CreatedPage, Dashboard, DisplayPage, DoSearchPage, SigninPage.
	- Resources:		Contain the resource files:
		+ Testcase_Info.xlsx:	you can add the information of issue to this file and the script will read from it and input to test site.
	- Utilities:
		+ Browsers.java:	where you can define the browsers, version to run the test (you need to install web driver for each of them).
		+ ComFunctions.java:	where the common functions are defined.
		+ Configure.java:	where constant variables are defined.
		+ Excel.java:	define functions to read/write excel file.
		+ Setup.java:	define setup and tear-down for script.
b. libs: all the external jars files which are used will be added here.

############
# 3. Setup #
############

a. You need to add external jars to build path:
	- Open project from Eclipse.
	- Righ click on Project > Select Properties > Java build path. Then navigate to Libraries tab and click Add External JARs.
	- Browse to "libs" folder in "src" folder and add all jars.

b How to run:
	- Open project from Eclipse. Go to src -> Resources -> Testcase_Info.xlsx
	Or open excel file C:\Users\DongFMV\workspace\AtlassTest\src\Resources\Testcase_Info.xlsx
		login sheet: Update account info to sign-in confluence page.
		CreatePage sheet: Update info in columns TC, pagetype, title, content, expect and leave columns actual and result blank.
		RestrictPage sheet: Update info in columns TC, pagesearch, view, edit, viewuser, edituser and expected leave columns result blank.
	- Go to TC_CreatePage and TC_RestrictPage. Put TC number in array int[] tcCreatePage = {1,2,3,4};
	- Run TC_RunAll.java
		
	  				
#################
# 4. Limitation #
#################
There are some limit when using this script to create a page:
	- The script has not been coded to handle case when create a page with an exiting name page.
	
		
			