package automationTest;
import PageObjects.CreatedPage;
import PageObjects.Dashboard;
import PageObjects.DisplayPage;
import PageObjects.Dashboard.PageType;
import PageObjects.SigninPage;
import PageObjects.DoSearchPage;
import Utilities.*;

public class TC_RestrictPage {

	public static void main(String[] args) throws Exception {
		//Input TC_Login# 
		int tcLogins = 1;
		Setup.setupLogin();
		SigninPage.runLogin(tcLogins);
		
		//Input TC_CreatePage# want to run 
		int[] tcRestrictPage = {1,2,3,4};
		int i=0;

		while ( i < tcRestrictPage.length )
		{
			ComFunctions.goHome();
			Setup.setupSearch();
			Dashboard.search(tcRestrictPage[i]);
			DisplayPage.editPage(tcRestrictPage[i]);
			i++;
		}

		Setup.closePage();
	}

}