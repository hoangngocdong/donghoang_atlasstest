package automationTest;
import java.lang.reflect.Array;

import PageObjects.CreatedPage;
import PageObjects.Dashboard;
import PageObjects.DisplayPage;
import PageObjects.Dashboard.PageType;
import PageObjects.SigninPage;
import Utilities.*;

public class TC_CreatePage {

	public static void main(String[] args) throws Exception {
		
		//Input TC_Login# 
		int tcLogins = 1;
		Setup.setupLogin();
		SigninPage.runLogin(tcLogins);
		
		//Input TC_CreatePage# want to run 
		int[] tcCreatePage = {1,2,3,4};
		int i=0;
		while ( i < tcCreatePage.length )
		{			
			//if success
			Setup.setupCreatePage();
						Dashboard.openPage(tcCreatePage[i]);
			//if successful go to DisplayPage
			DisplayPage.displayPage(tcCreatePage[i]);
			i++;
		}
		
		Setup.closePage();
	}

}