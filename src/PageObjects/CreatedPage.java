package PageObjects;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import PageObjects.Dashboard.PageType;
import Utilities.*;


public class CreatedPage {

	private static int pageTypeCol = 1;
	private static int titleCol = 2;
	
	public static WebElement contentTitle(){
		return ComFunctions.getElement(Configure.How.ID, "content-title");
	}
	
	public static WebElement contentBody(){
		return ComFunctions.getElement(Configure.How.ID, "wysiwygTextarea");
	}

	public static WebElement restrictions(){
		return ComFunctions.getElement(Configure.How.ID, "rte-button-restrictions");
	}
	
	public static WebElement save(){
		return ComFunctions.getElement(Configure.How.ID, "rte-button-publish");
	}
	
	public static WebElement close(){
		return ComFunctions.getElement(Configure.How.ID, "rte-button-cancel");
	}

	public static WebElement blankPage(){
		return ComFunctions.getElement(Configure.How.XPATH, 
											 "//li[@data-item-module-complete-key='com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blank-page']");
	}
	
	public static WebElement blogPost(){
		return ComFunctions.getElement(Configure.How.XPATH, 
											 "//li[@data-item-module-complete-key='com.atlassian.confluence.plugins.confluence-create-content-plugin:create-blog-post']");	
	}
	
	public static WebElement btSubmitPage(){
		return ComFunctions.getElement(Configure.How.XPATH, "//button[@class='create-dialog-create-button aui-button aui-button-primary']");
	}
	
	public static void createPage(int rowIndex) throws Exception{
		String pageType = Excel.getCellData(rowIndex, pageTypeCol);
		
		switch(pageType) {
			case PageType.BLANKPAGE:
				blankPage().click();
				//Write a command to Verify blankpage will be opened
			break;
			
			case PageType.BLOGPOST:
				blogPost().click();
				//Write a command to Verify blogPost will be opened
			break;	
				
		}
		
		btSubmitPage().click();
		editPage(rowIndex);
	} 
	
	public static void editPage(int rowIndex) throws Exception
	{
		String title = Excel.getCellData(rowIndex, titleCol);
		contentTitle().sendKeys(title);	
		//String content = DH_ExcelUtils.getCellData(1, 5);
		//contentBody().sendKeys(content);
		save().click();
	}	
}
