package PageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Utilities.ComFunctions;
import Utilities.Configure;
import Utilities.Excel;
import Utilities.Setup;

public class DisplayPage {
	private static int expectedCol = 4;
	private static int actualCol = 5;
	private static int resultCol = 6;
	
	private static String _viewMode;
	private static String _editMode;
	private static String _userView;
	private static String _userEdit;

	private static int viewCol = 2;
	private static int editCol = 3;
	private static int userViewCol = 4;
	private static int userEditCol = 5;
	private static int restrictResultCol = 6;
	
	public static WebElement cancelButton(String linkText){
		return ComFunctions.getElement(Configure.How.LINKTEXT, linkText);
	}
	public static WebElement lockedRestrict(){
		return ComFunctions.getElement(Configure.How.CSSSELECTOR, "span.icon icon-restricted");
	}
	
	public static WebElement appliedRestrict(){
		return ComFunctions.getElement(Configure.How.ID, "content-metadata-page-restrictions");
	}
	
	public static WebElement inputtUser(){
		return ComFunctions.getElement(Configure.How.ID, "page-permissions-names-input");
	}
	
	public static WebElement permissionViewType()
	{
		List<WebElement> elements = Setup._webdriver.findElements(By.className("first-of-type"));
		WebElement viewType = null;
		if ( elements != null && elements.size() > 0)
		{
			viewType = elements.get(0);
		}
		return viewType;
	}

	public static WebElement permissionEditType()
	{
		int index =  ( _viewMode.equals("YES") ) ? 1 : 0;
		List<WebElement> elements = Setup._webdriver.findElements(By.className("first-of-type"));
		WebElement viewType = null;
		if ( elements != null && elements.size() > index)
		{
			viewType = elements.get(index);
		}
		return viewType;
	}
	
	public static WebElement permissionViewUser(){
		List<WebElement> elements = Setup._webdriver.findElements(By.className("permission-entity-display-name"));
		WebElement viewPermission = null;
		if ( elements != null && elements.size() > 0)
		{
			viewPermission = elements.get(0);
		}

		return viewPermission;
	}

	public static WebElement permissionEditUser(){
		int index =  ( _viewMode.equals("YES") ) ? 1 : 0;
		List<WebElement> elements = Setup._webdriver.findElements(By.className("permission-entity-display-name"));
		WebElement viewPermission = null;
		if ( elements != null && elements.size() > index)
		{
			viewPermission = elements.get(index);
		}

		return viewPermission;
	}
	
	public static WebElement restrictUser(){
		return ComFunctions.getElement(Configure.How.ID, "add-typed-names");
	}
	
	public static WebElement saveRestrict(){
		return ComFunctions.getElement(Configure.How.CSSSELECTOR, "button.button-panel-button.permissions-update-button");
	}
	
	
	public static WebElement disTitle(){
		return ComFunctions.getElement(Configure.How.ID, "title-text");
	}
	
	public static WebElement disRestrictDialog(){
		return ComFunctions.getElement(Configure.How.ID, "update-page-restrictions-dialog");
	}
	
	public static WebElement editPageLink(){
		return ComFunctions.getElement(Configure.How.ID, "editPageLink");
	}

	public static WebElement restrictViewRadio(){
		return ComFunctions.getElement(Configure.How.ID, "restrictViewRadio");
	}
	
	public static WebElement restrictEditRadio(){
		return ComFunctions.getElement(Configure.How.ID, "restrictEditRadio");
	}
	
	public static WebElement restrictButton(){
		return ComFunctions.getElement(Configure.How.ID, "rte-button-restrictions");
	}
	
	private static void readTestCase(int rowIndex) throws Exception
	{	
		_viewMode = Excel.getCellData(rowIndex, viewCol);
		_editMode = Excel.getCellData(rowIndex, editCol);
		_userView = Excel.getCellData(rowIndex, userViewCol);
		_userEdit = Excel.getCellData(rowIndex, userEditCol);
		
		System.out.println("_viewMode: " + _viewMode);
		System.out.println("_editMode: " + _editMode);
	}
	
	private static void restrictMode(int rowIndex) throws Exception
	{
		boolean isSuccessful = false;
		readTestCase(rowIndex);
		System.out.println("_viewMode: " + _viewMode);
		System.out.println("_editMode: " + _editMode);
		if ( _viewMode.equals("YES") )
		{
			if ( restrictViewRadio().isSelected() == false )
			{
				restrictViewRadio().click();
			}
			//add view user
			inputtUser().clear();
			inputtUser().sendKeys(_userView);
			restrictUser().click();
			if ( permissionViewType() != null && permissionViewType().getText().equals("Viewing restricted to:") )
			{
				if ( permissionViewUser() != null && permissionViewUser().getText().equals(_userView) )
				{
					isSuccessful = true;
				}
			}
		}

		if ( isSuccessful || !_viewMode.equals("YES") )
		{
			if (_editMode.equals("YES"))
			{
				if (  restrictEditRadio().isSelected() == false )
				{
					restrictEditRadio().click();
				}
				//add edit user
				inputtUser().clear();
				inputtUser().sendKeys(_userEdit);
				restrictUser().click();
				
				if ( permissionEditType() != null && permissionEditType().getText().equals("Editing restricted to:") )
				{
					if ( permissionEditUser() != null && permissionEditUser().getText().equals(_userEdit) )
					{
						isSuccessful = true;
					}
					else
					{
						isSuccessful = false;
					}
				}
				else
				{
					isSuccessful = false;
				}
			}
		}

		if ( isSuccessful )
		{
			saveRestrict().click();
			outputRestrictResult(rowIndex);
		}
		else
		{
			WebElement element = null;
			try
			{
				element = cancelButton("Close");
				if ( element == null )
				{
					element = cancelButton("Cancel");
				}
				
			}
			catch(Exception ex)
			{
				element = cancelButton("Cancel");
			}
	
			if ( element != null )
			{
				element.click();
			}

			Excel.setCellData("FAILED", rowIndex, restrictResultCol);
		}
		
		if ( CreatedPage.save() != null )
		{
			CreatedPage.save().click();
		}
	}
	

	public static void opeResrticDialog(int rowIndex) throws Exception
	{
		ComFunctions.waitUntilElementVisible(restrictViewRadio());
		restrictMode(rowIndex);
	}
	
	public static void editPage(int rowIndex) throws Exception
	{
		editPageLink().click();
		ComFunctions.waitUntilElementVisible(restrictButton());
		restrictButton().click();
		opeResrticDialog(rowIndex);
	}
	
	private static void outputRestrictResult(int rowIndex) throws Exception
	{
		ComFunctions.waitUntilElementVisible(restrictButton());
		WebElement foundElement = restrictButton().findElement(By.className("trigger-text"));
		if ( foundElement != null && foundElement.getText().equals("Restricted") )
		{
			Excel.setCellData("PASSED", rowIndex, restrictResultCol);
			/*
			DH_CreatedPage.save();
			DH_CommonFunctions.waitUntilElementVisible(disTitle());
			WebElement appliedRestrict = appliedRestrict();
			if ( appliedRestrict != null )
			{
				DH_ExcelUtils.setCellData("PASSED", rowIndex, restrictResultCol);
			}
			else
			{
				DH_ExcelUtils.setCellData("FAILED", rowIndex, restrictResultCol);
			}
			*/
		}
		else
		{
			Excel.setCellData("FAILED", rowIndex, restrictResultCol);
		}
	}
	
	public static void displayPage(int rowIndex) throws Exception
	{
		ComFunctions.waitUntilElementVisible(disTitle());
		WebElement child = disTitle().findElement(By.tagName("a"));
		//System.out.println("child: " + child.getText());
		String Expectedtitle = Excel.getCellData(rowIndex, expectedCol);
		//System.out.println("Expectedtitle: " + Expectedtitle);
		Excel.setCellData(child.getText(), rowIndex, actualCol);
		String Actualtitle = Excel.getCellData(rowIndex, actualCol);
		
		if( Expectedtitle.equals(Actualtitle) )
		{
			Excel.setCellData("PASSED", rowIndex, resultCol);
		}
		else
		{
			Excel.setCellData("FAILED", rowIndex, resultCol);
		}
			
	}
	
}
