package PageObjects;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import Utilities.*;


public class Dashboard {

	private static int pageSearchCol = 1;
	private static String _textSearch;
	public static class PageType {
		public static final String BLANKPAGE = "Blank page";
		public static final String BLOGPOST = "Blog post";
		public static final String DECISION = "Decision";
	
	}
	
	public static WebElement pageTitle(){
		return ComFunctions.getElement(Configure.How.ID, "WelcometoConfluence");
	}
	
	public static WebElement welcome(){
		return ComFunctions.getElement(Configure.How.ID, "WelcometoConfluence");
	}
	
	public static WebElement inputSearch(){
		return ComFunctions.getElement(Configure.How.ID, "quick-search-query");
	}
	
	public static WebElement btCreatePage(){
		//return DH_CommonFunctions.getElement(DH_Constant.How.CSSSELECTOR, "#create-page-button > span");
		return ComFunctions.getElement(Configure.How.ID, "create-page-button");
	}
	
	public static WebElement digCreatePage(){
		return ComFunctions.getElement(Configure.How.ID, "create-dialog");
		
	}

	public static void openPage(int rowIndex) throws Exception
	{
		btCreatePage().click();
		ComFunctions.waitUntilElementVisible(digCreatePage());
		//
		CreatedPage.createPage(rowIndex);
	}
	
	private static void readTestCase(int rowIndex) throws Exception
	{
		_textSearch = Excel.getCellData(rowIndex, pageSearchCol);
	}

	public static void search(int rowIndex) throws Exception
	{
		readTestCase(rowIndex);
		inputSearch().sendKeys(_textSearch);
		inputSearch().sendKeys(Keys.ENTER);
		//
		DoSearchPage.goToFoundPage(_textSearch);
	}
	
}
