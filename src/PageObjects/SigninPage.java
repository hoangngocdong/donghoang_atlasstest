package PageObjects;

//import org.junit.Assert;
import Utilities.ComFunctions;
import Utilities.Configure;




import Utilities.Excel;

import org.openqa.selenium.WebElement;

public class SigninPage {
	
	private static int urlCol = 1;
	private static int userNameCol = 2;
	private static int passCol = 3;
	//
	private static String _url;
	private static String _userName;
	private static String _pass;
	
	public static WebElement btSignIn() 
	{		
		return ComFunctions.getElement(Configure.How.ID, "login");
	}
	
	public static WebElement txtUserName() 
	{
		return ComFunctions.getElement(Configure.How.ID, "username");
	}
	
	public static WebElement txtPassWord() 
	{
		return ComFunctions.getElement(Configure.How.ID, "password");
	}
	
	public static WebElement chxStaySignedIn() 
	{
		return ComFunctions.getElement(Configure.How.ID, "remember-me");			
	}
	
	public static void signIn(String userName, String passWord, boolean isRememberMe) 
	{		
		txtUserName().sendKeys(userName);
		txtPassWord().sendKeys(passWord);
		if((isRememberMe && !chxStaySignedIn().isSelected()) ||
		   (!isRememberMe && chxStaySignedIn().isSelected())) 
		{
			chxStaySignedIn().click();
		}
		
		btSignIn().click();		
	}
	
	private static void login()
	{
		System.out.println("_url: " + _url);
		ComFunctions.goToUrl(_url);
		System.out.println("_userName: " + _userName);
		System.out.println("_pass: " + _pass);
		SigninPage.signIn(_userName, _pass, true);
	}
	
	private static void readTestCase(int rowIndex) throws Exception
	{
		_url = Excel.getCellData(rowIndex, urlCol);
		System.out.println("_url: " + _url);
		_userName = Excel.getCellData(rowIndex, userNameCol);
		System.out.println("_userName: " + _userName);
		_pass = Excel.getCellData(rowIndex, passCol);
		System.out.println("_pass: " + _pass);
	}
	
	public static void runLogin(int rowIndex) throws Exception
	{
		readTestCase(rowIndex);
		login();
	}
}
