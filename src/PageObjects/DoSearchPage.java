package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Utilities.ComFunctions;
import Utilities.Configure;
import Utilities.Excel;

public class DoSearchPage {

	public static WebElement getFoundPageLink(String foundName){
		return ComFunctions.getElement(Configure.How.LINKTEXT, foundName);
	}
	
	public static void goToFoundPage(String foundName) throws Exception
	{
		if ( getFoundPageLink(foundName) != null )
		{
			getFoundPageLink(foundName).click();
		}
			
	}
	
}
