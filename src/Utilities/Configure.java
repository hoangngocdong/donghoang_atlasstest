package Utilities;

public class Configure {
	public static class CoBrand {
		public static final String DHCONF = "https://donghoang.atlassian.net/login";
		/*
		 We can define more environments at here. For instance: beta, production 
		*/		
	}
	
	public static class How {
		public static final String ID = "id";
	    public static final String NAME = "name";
	    public static final String TAGNAME = "tagName";
	    public static final String CLASSNAME = "className";
	    public static final String CSSSELECTOR = "cssSelector";
	    public static final String LINKTEXT = "linkText";
	    public static final String PARTIALLINKTEXT = "paritalLinkText";
	    public static final String XPATH = "xpath";
	    public static final String CUSTOM = "custom";
	}
	
	public static final String Path_TestData = "/src/Resources/";
	public static final String File_TestData = "Testcase_Info.xlsx";	
}

