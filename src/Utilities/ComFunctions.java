package Utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;



public class ComFunctions {

	public static void goToUrl(String url) {
		Setup._webdriver.navigate().to(url);
	}
	
	public static WebElement getElement(String how, String query) {
		WebElement element = null;		
		Setup._webdriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		switch(how) {
			case "id" :
				element = Setup._webdriver.findElement(By.id(query));
				break;				
			case "linkText":
				element = Setup._webdriver.findElement(By.linkText(query));
				break;
			case "name":
				element = Setup._webdriver.findElement(By.name(query));
				break;
			case "className":
				element = Setup._webdriver.findElement(By.className(query));
				break;
			case "xpath":
				element = Setup._webdriver.findElement(By.xpath(query));
				break;
			case "cssSelector":
				element = Setup._webdriver.findElement(By.cssSelector(query));
				break;
			//You can define more query at here...			
		}
		return element;
		
	}
	
	public static void waitUntilElementVisible(WebElement element) {
		WebDriverWait wait = new WebDriverWait(Setup._webdriver, 10);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public static void goHome(){
		WebElement homeElement = getElement(Configure.How.CSSSELECTOR, "img[alt=\"Confluence\"]");
		homeElement.click();
	}
}
