package Utilities;

import java.io.File;
import java.io.FileInputStream;


import java.io.FileOutputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Excel{
	private static XSSFSheet ExcelWSheet;		 
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	private static XSSFRow Row;
	private static String _path;
	private static FileInputStream ExcelFile;
	
	public static void setExcelFile(String Path,String SheetName) throws Exception {			 
		try {
			String currentPath = new File(".").getCanonicalPath();
			_path = currentPath + Path;
			System.out.println("_path: " + _path);
			ExcelFile = new FileInputStream(_path);
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);

		} catch (Exception e){
			throw (e);
		}
	}
	
	public static void setCellData(String Result,  int RowNum, int ColNum) throws Exception	{
		 
			try{

  			Row  = ExcelWSheet.getRow(RowNum);

			Cell = Row.getCell(ColNum, Row.RETURN_BLANK_AS_NULL);

			if (Cell == null) 
			{

				Cell = Row.createCell(ColNum);

				Cell.setCellValue(Result);

			} 
			else 
			{
				Cell.setCellValue(Result);

			}

			// Constant variables Test Data path and Test Data file name

			FileOutputStream fileOut = new FileOutputStream(_path);

			ExcelWBook.write(fileOut);

			fileOut.flush();

				fileOut.close();

			}catch(Exception e){

				throw (e);

			}

		}

	
	public static String getCellData(int RowNum, int ColNum) throws Exception{			 
		try 
		{
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = Cell.getStringCellValue();
			return CellData;
		} catch (Exception e){
			return"";
		}
    }
	
	
}
