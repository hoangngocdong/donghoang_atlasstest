package Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import Utilities.Excel;
import Utilities.Configure;
import PageObjects.*;

public class Setup {
	//The WEB DRIVER
	public static WebDriver _webdriver;
	
	//Default value for local testing
	public static String _browserName = Browsers.Firefox.Name;

	public static void initWebDriver () {
		switch(_browserName) {
			case Browsers.Firefox.Name:
				_webdriver = new FirefoxDriver();
				break;
			case Browsers.Chrome.Name:
				_webdriver = new ChromeDriver();
				break;
			case Browsers.InternetExplorer.Name:
				_webdriver = new InternetExplorerDriver();
		}
	}

	
	public static void setupLogin() throws Exception {
		// setup path of excel file 
		initWebDriver();
		_webdriver.manage().window().maximize();
		Excel.setExcelFile(Configure.Path_TestData + Configure.File_TestData,"Login");
//		//DH_CommonFunctions.waitUntilElementVisible(welcome());
	}

	public static void setupCreatePage() throws Exception {
		Excel.setExcelFile(Configure.Path_TestData + Configure.File_TestData,"CreatePage");
	}
	
	public static void setupSearch() throws Exception {
		// setup path of excel file 
		Excel.setExcelFile(Configure.Path_TestData + Configure.File_TestData,"RestrictPage");
	}
	
	public static void closePage() {
		//We can define logOut function at here
		_webdriver.close();
	}
	
}
